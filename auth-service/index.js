const express = require("express");
const app = express();
const port = process.env.PORT || 3003;

const index = require("./src/routes");

app.use(express.json({ extended: false }));

app.use("/", index);

app.listen(port, () => {
  console.log(`Server started on port: ${port}`);
});
