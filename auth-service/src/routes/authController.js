const jwt = require("jsonwebtoken");

const loginController = async (req, res) => {
  try {
    const { token } = req.body;

    if (!token) {
      return res.status(401).json({ msg: "No token, auth denied" });
    }

    const decoded = jwt.verify(token, "jwtsecretwhocares");

    res.status(200).json({ decoded });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server error");
  }
};

module.exports = loginController;
