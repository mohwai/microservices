const express = require("express");

const authController = require("./authController");

const router = express.Router();

router.post("/auth", authController);

module.exports = router;
