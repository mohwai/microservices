const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

const mongoose = require("mongoose");

const index = require("./controllers");

(async () => {
  try {
    await mongoose.connect(
      "mongodb+srv://mogwai:mogwai@microservice-labs.huloe.mongodb.net/microservice-labs?retryWrites=true&w=majority",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
      }
    );

    console.log("MongoDB connected.");
  } catch (err) {
    console.error(err.message);
    process.exit(1);
  }
})();

app.use(express.json({ extended: false }));

app.use("/", index);

app.listen(port, () => {
  console.log(`Server started on port: ${port}`);
});
