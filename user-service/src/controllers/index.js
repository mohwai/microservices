const express = require("express");

const loginController = require("./loginController");
const signupController = require("./signupController");

const router = express.Router();

router.post("/login", loginController);
router.post("/signup", signupController);

module.exports = router;
