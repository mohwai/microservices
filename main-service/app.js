const express = require("express");
const bodyParser = require("body-parser");
const index = require("./routes");

const app = express();

app.use(bodyParser.json());

app.use("/", index);

module.exports = app;
