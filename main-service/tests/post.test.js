const request = require("supertest");
const app = require("../app");

describe("/posts/:id", () => {
  // describe = name of the test
  it("should fetch a post by id", async () => {
    // it ("should do something")
    const res = await request(app) // make a request to local endpoint
      .get("/post?id=1")
      .send({});

    expect(res.status).toEqual(201);
    expect(res.body).toHaveProperty("post");
  });
});
