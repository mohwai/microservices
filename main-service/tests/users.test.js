const request = require("supertest");
const app = require("../app");

describe("/users", () => {
  it("should fetch the users", async () => {
    const res = await request(app)
      .get("/users")
      .send({});

    expect(res.status).toEqual(201);
    expect(res.body).toHaveProperty("users");
  });
});
