const request = require("supertest");
const app = require("../app");

describe("/users/:id", () => {
  it("should fetch a user by id", async () => {
    const res = await request(app)
      .get("/user?id=1")
      .send({});

    expect(res.status).toEqual(201);
    expect(res.body).toHaveProperty("user");
  });
});
