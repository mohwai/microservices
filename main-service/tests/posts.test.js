const request = require("supertest");
const app = require("../app");

describe("/posts", () => {
  it("should fetch the posts", async () => {
    const res = await request(app)
      .get("/posts")
      .send({});

    expect(res.status).toEqual(201);
    expect(res.body).toHaveProperty("posts");
  });
});
