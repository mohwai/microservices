const fetch = require("node-fetch");

const postController = async (req, res) => {
  const { id } = req.query;

  const response = await fetch(
    `https://jsonplaceholder.typicode.com/posts/${id}`
  ).then(res => res.json());

  return res.status(201).json({ post: response });
};

module.exports = postController;
