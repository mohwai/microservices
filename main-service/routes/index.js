const express = require("express");

const postsController = require("./postsController");
const postController = require("./postController");
const usersController = require("./usersController");
const userController = require("./userController");

const router = express.Router();

router.get("/", (req, res) => res.send("not found"));
router.get("/posts", postsController);
router.get("/post", postController);
router.get("/users", usersController);
router.get("/user", userController);

module.exports = router;
