const fetch = require("node-fetch");

const userController = async (req, res) => {
  const { id } = req.query;

  const response = await fetch(
    `https://jsonplaceholder.typicode.com/users/${id}`
  ).then(res => res.json());

  return res.status(201).json({ user: response });
};

module.exports = userController;
