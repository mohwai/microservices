const fetch = require("node-fetch");

const postsController = async (req, res) => {
  const response = await fetch(
    "https://jsonplaceholder.typicode.com/posts"
  ).then(res => res.json());

  return res.status(201).json({ posts: response });
};

module.exports = postsController;
