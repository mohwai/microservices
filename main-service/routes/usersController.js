const fetch = require("node-fetch");

const usersController = async (req, res) => {
  const response = await fetch(
    "https://jsonplaceholder.typicode.com/users"
  ).then(res => res.json());

  return res.status(201).json({ users: response });
};

module.exports = usersController;
